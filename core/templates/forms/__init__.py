from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Length
from flask_wtf import CSRFProtect

csrf = CSRFProtect()

class Login(FlaskForm):
    Username = StringField('Username', [DataRequired()])
    Password = StringField('Password', [DataRequired()])
    submit = SubmitField('Submit')