from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class TestSession(db.Model):

    def __init__(self, random_record):
        self.random_record = random_record

    __tablename__ = 'Test-record'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    random_record = db.Column(db.String(64), index=True)


class UserAccess(db.Model):
    __tablename__ = 'User-Accounts'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(128))
    session_id = db.Column(db.String(64), unique=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    role = db.Column(db.Integer, index=True)


class Anonymous(db.Model):
    __tablename__ = 'Anonymous-Accounts'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    session_id = db.Column(db.String(64), index=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)


class Roles(db.Model):
    __tablename__ = 'Website-Access-Roles'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role_id = db.Column(db.String(64))
    role_title = db.Column(db.String(64))


class ActivityLog(db.Model):

    def __init__(self, a, b, c):
        self.session_id = a
        self.time_stamp = b
        self.message = c

    __tablename__ = 'Anonmymous-Activity-Log'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    session_id = db.Column(db.String(64), index=True)
    message = db.Column(db.String(64), index=True)
    page_visited = db.Column(db.String(64), index=True)
    time_stamp = db.Column(db.DateTime(32))
    ip_address = db.Column(db.String(64))
    geographic = db.Column(db.String(64))
    cooke_set = db.Column(db.String(128))


class LogCreation(object):

    def __init__(self, session_id=None, message=None):
        self.db_conn(session_id, message)

    def db_conn(self, session_id, message):
        tbl1 = ActivityLog(session_id, datetime.now(), message)
        db.session.add(tbl1)
        db.session.commit()


class WebsiteContentCreation(db.Model):
    def __init__(self, a=None, b=None, c=None, d=None):
        self.pageTitle = a
        self.pageName = b
        self.pageParent = c
        self.pageURL = d

    __tablename__ = 'Website-Content-Creation'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    pageTitle = db.Column(db.String(64))
    pageName = db.Column(db.String(64))
    pageParent = db.Column(db.String(64))
    pageURL = db.Column(db.String(64))


'''
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    html_content_function = db.Column(db.String(64), index=True)
    html_id = db.Column(db.String(64), index=True)
    html_content_code = db.Column(db.String(256))
    html_style = db.Column(db.String(64))
    url_page_name = db.Column(db.String(64), index=True)
'''


class MetaTags(db.Model):
    __tablename__ = 'Meta-Website-Tags'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    meta_description = db.Column(db.String(128), index=True)
    meta_keywords = db.Column(db.String(128))
    meta_author = db.Column(db.String(128), index=True)
    meta_viewport = db.Column(db.String(128), index=True)


class MetaLinks(db.Model):
    __tablename__ = 'Meta-Link-Connection'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    html_content_id = db.Column(db.ForeignKey('HTML-Website-Content.id'))
    html_meta_tags = db.Column(db.ForeignKey('Meta-Website-Tags.id'))
