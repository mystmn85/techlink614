from flask import Flask, render_template, request, url_for, redirect, jsonify, Markup, escape
from .models import TestSession, LogCreation, ActivityLog, WebsiteContentCreation, db
from .routes import HTMLContent_form, FormProtection
from .templates import forms
import random

''' FOLDER IMPORT '''
from .config import Configuration, Configurationdb

''' DEFINE APP '''
app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
WTF_CSRF_SECRET_KEY = 'SFDLNSDFMKL;SDFKLM;FLKMWDfjiwqeoir2q34or092u9rfmkawdsfklvmls'

FormProtection.begin().init_app(app)
app.config.from_object(Configurationdb)
app.config.from_object(Configuration)
forms.csrf.init_app(app)

# DEFINE Database Connection
with app.app_context():
    db.init_app(app)


#    db.create_all()

# Custom functions or classes
def random_session():
    return random.randint(0, 1000000000)


@app.errorhandler(404)
def not_found(e):
    return render_template("404.html")


# DEFINE Routes
@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/p/login', methods=['GET', 'POST'])
def login():
    form = forms.Login()
    uN = str('')

    if request.method == 'GET':
        uN = str('GET Request')

    if form.validate_on_submit() and request.method == 'POST':
        # this block is only entered when the form is submitted
        uN = request.form['username'] if request.form['username'] == 'pcameron' else "{} - 1 ".format(
            request.form['username'])

    return render_template('login.html', msg=uN, form=form)
